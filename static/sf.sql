CREATE TABLE [problema] (
  [id] int PRIMARY KEY IDENTITY(1, 1),
  [ejercicio] nvarchar(255) NOT NULL,
  [respuesta_multiple] BIT NOT NULL DEFAULT 0
)
GO

CREATE TABLE [respuesta] (
  [id] bigint PRIMARY KEY IDENTITY(1, 1),
  [dato] nvarchar(255) NOT NULL,
  [correcta] BIT DEFAULT 0,
  [problema_id] int
)
GO

CREATE TABLE [examen] (
  [id] int PRIMARY KEY IDENTITY(1, 1),
  [fecha] datetime DEFAULT 'GETDATE',
  [estudiante] nvarchar(255) NOT NULL,
  [descripcion] nvarchar(255) NOT NULL,
  [duracion_segundos] int NOT NULL
)
GO

CREATE TABLE [ejercicio] (
  [id] bigint PRIMARY KEY IDENTITY(1, 1),
  [problema_id] int NOT NULL,
  [examen_id] int NOT NULL
)
GO

CREATE TABLE [respuesta_ejercicio] (
  [id] bigint PRIMARY KEY IDENTITY(1, 1),
  [correcta] BIT DEFAULT 0,
  [ejercicio_id] bigint NOT NULL
)
GO

ALTER TABLE [respuesta] ADD FOREIGN KEY ([problema_id]) REFERENCES [problema] ([id])
GO

ALTER TABLE [ejercicio] ADD FOREIGN KEY ([problema_id]) REFERENCES [problema] ([id])
GO

ALTER TABLE [ejercicio] ADD FOREIGN KEY ([examen_id]) REFERENCES [examen] ([id])
GO

ALTER TABLE [respuesta_ejercicio] ADD FOREIGN KEY ([ejercicio_id]) REFERENCES [ejercicio] ([id])
GO

CREATE TABLE "problema" (
  "id" SERIAL PRIMARY KEY,
  "ejercicio" varchar NOT NULL,
  "respuesta_multiple" boolean NOT NULL DEFAULT false
);

CREATE TABLE "respuesta" (
  "id" SERIAL PRIMARY KEY,
  "dato" varchar NOT NULL,
  "correcta" boolean DEFAULT false,
  "problema_id" int
);

CREATE TABLE "examen" (
  "id" SERIAL PRIMARY KEY,
  "fecha" datetime DEFAULT 'now',
  "estudiante" varchar NOT NULL,
  "descripcion" varchar NOT NULL,
  "duracion_segundos" int NOT NULL
);

CREATE TABLE "ejercicio" (
  "id" SERIAL PRIMARY KEY,
  "problema_id" int NOT NULL,
  "examen_id" int NOT NULL
);

CREATE TABLE "respuesta_ejercicio" (
  "id" SERIAL PRIMARY KEY,
  "correcta" boolean DEFAULT false,
  "ejercicio_id" bigint NOT NULL
);

ALTER TABLE "respuesta" ADD FOREIGN KEY ("problema_id") REFERENCES "problema" ("id");

ALTER TABLE "ejercicio" ADD FOREIGN KEY ("problema_id") REFERENCES "problema" ("id");

ALTER TABLE "ejercicio" ADD FOREIGN KEY ("examen_id") REFERENCES "examen" ("id");

ALTER TABLE "respuesta_ejercicio" ADD FOREIGN KEY ("ejercicio_id") REFERENCES "ejercicio" ("id");

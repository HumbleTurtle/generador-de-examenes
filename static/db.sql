CREATE TABLE IF NOT EXISTS `problema` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `ejercicio` TEXT NOT NULL,
  `esImagen` boolean NOT NULL DEFAULT 0,
  `respuesta_multiple` boolean NOT NULL DEFAULT 0,
  `categoria_id` INTEGER NOT NULL,
  FOREIGN KEY (categoria_id)
      REFERENCES categoria (id)
);

CREATE TABLE IF NOT EXISTS `respuesta` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `dato` TEXT NOT NULL,
  `correcta` boolean DEFAULT 0,
  `problema_id` INTEGER,
  FOREIGN KEY (problema_id)
       REFERENCES problema (id) 
);

CREATE TABLE IF NOT EXISTS `examen` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `fecha` datetime DEFAULT "now",
  `estudiante` TEXT NOT NULL,
  `descripcion` TEXT NOT NULL,
  `duracion_segundos` INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS `categoria` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `nombre` TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS `ejercicio` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `problema_id` INTEGER NOT NULL,
  `examen_id` INTEGER NOT NULL,
  FOREIGN KEY (examen_id)
      REFERENCES examen (id),
  FOREIGN KEY (problema_id)
    REFERENCES problema (id) 
);

CREATE TABLE IF NOT EXISTS `respuesta_ejercicio` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `correcta` boolean DEFAULT false,
  `ejercicio_id` INTEGEREGER NOT NULL,
  FOREIGN KEY (ejercicio_id)
    REFERENCES ejercicio (id)   
);

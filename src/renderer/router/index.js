import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/examen',
      name: 'examen',
      component: require('@/components/Examen').default 
    },

    {
      path: '/categorias',
      name: 'categorias',
      component: require('@/components/Categorias').default
    },

    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

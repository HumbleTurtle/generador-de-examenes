var problemas = function ( sqlhelper ) {

  var sqlhelper = sqlhelper

  var insertar = function (problema) {
    var problemaPrepare = sqlhelper.db.prepare('INSERT INTO problema (ejercicio,esImagen,respuesta_multiple,categoria_id) VALUES (?,?,?,?)')
    
    problemaPrepare.run(problema.ejercicio, problema.esImagen, problema.respuesta_multiple, problema.categoria_id, function (earr) {
      insertarRespuestas( this.lastID, problema.respuestas )
    })

    problemaPrepare.finalize()
  }

  var obtenerAleatorios = async function () {
    var rows = await new Promise( (resolve, fail) => {
        var problemas = sqlhelper.db.all('SELECT DISTINCT * FROM \'problema\' ORDER BY RANDOM() LIMIT 30 ', function (err,rows) {
            resolve(rows)
        })
    } ) 

    var output = []
    for( row of rows ) { 
        row.respuestas = await new Promise( (resolve, fail) => {
            sqlhelper.db.all('SELECT * from \'respuesta\' where respuesta.problema_id = (?)', row.id, function(err,respuestas) {
                resolve(respuestas)
            })
        })
    }

    return rows
  }

  var insertarRespuestas = function (problema_id, respuestas ) {
      var respPrepare = sqlhelper.db.prepare('INSERT INTO respuesta (dato,correcta,problema_id) VALUES (?,?,?)')
    
      for ( var i = 0; i < respuestas.length; i++) {
          respPrepare.run( respuestas[i].dato, respuestas[i].correcta,  problema_id )
      }

      respPrepare.finalize()
  }

  Object.assign( this, {
    insertar,
    obtenerAleatorios
  })  

}

module.exports = problemas
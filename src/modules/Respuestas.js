var respuestas = function ( sqlhelper ) {

  var sqlhelper = sqlhelper


  var insertar = function (problema_id, respuestas ) {
      var respPrepare = sqlhelper.db.prepare('INSERT INTO respuesta (dato,correcta,problema_id) VALUES (?,?,?)')
    
      for ( var i = 0; i < respuestas.length; i++) {
          respPrepare.run( respuestas[i].dato, respuestas[i].correcta,  problema_id )
      }

      respPrepare.finalize()
  }

  Object.assign( this, {
    insertar
  })  

}

module.exports = respuestas
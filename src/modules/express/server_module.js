const express = require('express')
var fs = require('fs')
var https = require('https')
const path = require('path')
var bodyParser = require('body-parser')
var EventEmitter = require('events')


function server_module( port=3000 ) {
  const app = express()
  var ee = new EventEmitter()  

  this.listen = function () {

    app.use('/static', express.static(__static+"/camera_app/"))
    app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}))
    app.use(bodyParser.json({ limit: '50mb' }))

    app.post('/post/image', (req, res) => {
      var imageBase64 = req.body.imageBase64;
      // console.log("Received image data! " + imageBase64)

      ee.emit("post:image", imageBase64+"")
    })

    https.createServer({
      key: fs.readFileSync( path.join( __static, 'server.key'), 'utf8' ),
      cert: fs.readFileSync( path.join(__static, 'server.cert'), 'utf8' ),
      passphrase: ''
    }, app)
      .listen(port, () => console.log(`Example app listening on port ${port}!`))  
  }

  this.on = function( eventName, action) {
    ee.on(eventName, action)
  }
  
}

module.exports = server_module




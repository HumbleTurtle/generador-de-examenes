const sqlite3 = require('sqlite3').verbose()
const { dialog } = require('electron').remote

const Categorias = require("./Categorias")
const Respuestas = require("./Respuestas")
const Problemas  = require("./Problemas")


var dbFilename = null;
var listeners = []

var sqlhelper = {
  db: null
}

var newDB = function () {

    var chosenDBPath = dialog.showSaveDialog({
        filters:[
            { name: "Database", extensions: ["db"] }
          ],
    })

    if( chosenDBPath )
        chooseDB( chosenDBPath )

}

var chooseDB = function (chosenPath=null) {

    if (chosenPath == null)
        var chosenDBPath = elegirBaseDatos()
    else 
        var chosenDBPath = chosenPath


    if ( chosenDBPath == undefined )
        return

    var db = new sqlite3.Database(chosenDBPath)

    var limiterIndex = chosenDBPath.lastIndexOf('/')

    if ( limiterIndex == -1 ) {
        
        limiterIndex = chosenDBPath.lastIndexOf('\\')

        if ( limiterIndex == -1) {
            dbFilename = chosenDBPath
        } else {
            dbFilename = chosenDBPath.substring(limiterIndex+1);
        }
    } else {
        dbFilename = chosenDBPath.substring(limiterIndex+1);
    }

    sqlhelper.db = db

    initialize() 

    notifyListeners("changed:dbFilename", dbFilename)
    return db
}

function initialize () {
    var db = sqlhelper.db

    db.serialize(function () {
        const fs = require('fs')
        const path = require('path')
        var contents = fs.readFileSync(path.join(__static, 'db.sql'), 'utf8')

        db.exec(contents)
    }) 

    return db
}

var validState = function () {
    return sqlhelper.db !== null
}

var notifyListeners = function (event, data) {

    for ( var listener of listeners ) {
        console.log("Notifying listeners")
        listener(event, data)        
    }

}

var removeListener = function ( listener ) {
    listeners.splice( listeners.indexOf(listener), 1 )
}

var getDBFilename = function () {
    return dbFilename
}

var addListener = function( listener ) {
    listeners.push( listener )
    console.log(listener.length)
}


var elegirBaseDatos = function () {
    var db = dialog.showOpenDialog(
        { 
          filters:[
            { name: "Database", extensions: ["db"] }
          ],
          properties: ['openFile'] 
        }
    )
    if( db == undefined )
        return undefined
    else
        return db[0]

}


var methods = {
    newDB,
    chooseDB,

    validState,
    addListener,
    
    getDBFilename,
    removeListener,

    categorias: new Categorias( sqlhelper ),
    problemas: new Problemas( sqlhelper ),
    respuestas: new Respuestas( sqlhelper )
}

Object.assign(sqlhelper, methods)

module.exports = sqlhelper

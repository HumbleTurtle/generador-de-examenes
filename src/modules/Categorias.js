var categorias = function ( sqlhelper ) {

  var sqlhelper = sqlhelper

  var insertar = async function (categoria) {

    await new Promise( (resolve,reject) => {
        sqlhelper.db.run('INSERT INTO categoria (nombre) VALUES (?)', categoria.nombre, function (err) {
            console.log(err)

            resolve()
        })
    })

  }

  var obtener = async function () {
    
    var categorias = await new Promise( (resolve, reject) => {
        sqlhelper.db.all('SELECT * FROM categoria ORDER BY ID ASC', function(err, result) {
            resolve(result)
        })
    })

    console.log(categorias)
    return categorias;
  }

  var eliminar = async function(categoria) {
    await new Promise( (resolve,reject) => {
        sqlhelper.db.run('DELETE FROM categoria WHERE id = (?)', categoria.id,  function (err) {
            console.log(err)
            resolve()
        })
    })
  }

  Object.assign( this, {
    insertar,
    obtener,
    eliminar
  })  

}

module.exports = categorias